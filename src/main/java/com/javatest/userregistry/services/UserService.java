package com.javatest.userregistry.services;

import com.javatest.userregistry.models.UserEntity;

public interface UserService {
    UserEntity registerUser(UserEntity user);
}
