package com.javatest.userregistry.services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class JwtServiceImpl implements JwtService {
    private String secret;

    public JwtServiceImpl(@Value("${jwt.secret}") String secret) {
        this.secret = secret;
    }

    public String generateToken(String username, Date issuedAt) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, username, issuedAt);
    }

    private String createToken(Map<String, Object> claims, String subject, Date issuedAt) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(issuedAt)
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

}
