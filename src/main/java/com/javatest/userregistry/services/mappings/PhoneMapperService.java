package com.javatest.userregistry.services.mappings;

import com.javatest.userregistry.dto.PhoneDto;
import com.javatest.userregistry.models.PhoneEntity;
import org.springframework.stereotype.Service;

@Service
public class PhoneMapperService implements MapperService<PhoneDto, PhoneEntity> {
	@Override
	public PhoneDto convertToDto(PhoneEntity entity) {
		return entity == null ? null :
				new PhoneDto(entity.getId(),entity.getNumber(),entity.getCityCode(),entity.getCountryCode());
	}

	@Override
	public PhoneEntity convertToEntity(PhoneDto dto) {
		return dto == null ? null :
				new PhoneEntity(dto.getId(),dto.getNumber(),dto.getCityCode(),dto.getCountryCode());
	}
}
