package com.javatest.userregistry.services.mappings;

public interface MapperService<T, S> {
	T convertToDto(S entity);
	S convertToEntity(T dto);
}
