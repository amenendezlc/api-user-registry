package com.javatest.userregistry.services.mappings;

import com.javatest.userregistry.dto.PhoneDto;
import com.javatest.userregistry.dto.UserDto;
import com.javatest.userregistry.models.PhoneEntity;
import com.javatest.userregistry.models.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserMapperService implements MapperService<UserDto, UserEntity> {

	private MapperService<PhoneDto, PhoneEntity> phoneMapperService;

	public UserMapperService(MapperService<PhoneDto, PhoneEntity> phoneMapperService) {
		this.phoneMapperService = phoneMapperService;
	}

	@Override
	public UserDto convertToDto(UserEntity entity) {
		if(entity == null) {
			return null;
		} else {
			List<PhoneDto> phoneDtos = null;

			if (entity.getPhones() != null && !entity.getPhones().isEmpty()) {
				phoneDtos = entity.getPhones().stream().map(p ->
						phoneMapperService.convertToDto(p)
				).collect(Collectors.toList());
			}

			return new UserDto(entity.getId(), entity.getName(), entity.getEmail(),
					entity.getPassword(), phoneDtos, entity.getCreated(), entity.getModified(),
					entity.getLastLogin(), entity.getToken(), entity.isActive());

		}
	}

	@Override
	public UserEntity convertToEntity(UserDto dto) {
		if(dto == null) {
			return null;
		} else {
			List<PhoneEntity> phoneEntities = null;

			if (dto.getPhones() != null && !dto.getPhones().isEmpty()) {
				phoneEntities = dto.getPhones().stream().map(p ->
						phoneMapperService.convertToEntity(p)
				).collect(Collectors.toList());
			}

			return new UserEntity(dto.getId(), dto.getName(), dto.getEmail(),
					dto.getPassword(), phoneEntities, dto.getCreated(), dto.getModified(),
					dto.getLastLogin(), dto.getToken(), dto.isActive());
		}
	}
}
