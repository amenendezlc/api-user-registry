package com.javatest.userregistry.services;

import java.util.Date;

public interface JwtService {
    String generateToken(String username, Date issuedAt);
}
