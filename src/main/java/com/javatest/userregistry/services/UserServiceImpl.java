package com.javatest.userregistry.services;

import com.javatest.userregistry.exceptions.UserAlreadyExistsException;
import com.javatest.userregistry.models.UserEntity;
import com.javatest.userregistry.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private JwtService tokenService;

    public UserServiceImpl(UserRepository userRepository, JwtService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    @Override
    public UserEntity registerUser(final UserEntity user) {
        userExists(user);

        user.setCreated(new Date());
        user.setModified(new Date());
        user.setLastLogin(new Date());
        user.setActive(true);
        user.setToken(tokenService.generateToken(user.getEmail(), user.getCreated()));

        UserEntity registeredUser = userRepository.save(user);

        log.info("User created successfully! With ID {} and name {} and email {}",
                registeredUser.getId(),
                registeredUser.getName(),
                registeredUser.getEmail());

        return registeredUser;
    }

    private void userExists(UserEntity user) {
        userRepository.findByEmail(user.getEmail())
                .ifPresent(
                        t -> {throw new UserAlreadyExistsException("email", t.getEmail());});
    }

}