package com.javatest.userregistry.controllers;

import com.javatest.userregistry.dto.UserDto;
import com.javatest.userregistry.models.UserEntity;
import com.javatest.userregistry.services.UserService;
import com.javatest.userregistry.services.mappings.MapperService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController {

    private UserService userService;

    private MapperService<UserDto, UserEntity> userMapperService;

    public UserController(UserService userService, MapperService<UserDto, UserEntity> userMapperService) {
        this.userService = userService;
        this.userMapperService = userMapperService;
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public UserDto create(@RequestBody @Valid UserDto userDto) {
        UserEntity createdUser = userService.registerUser(userMapperService.convertToEntity(userDto));
        return userMapperService.convertToDto(createdUser);
    }
}
