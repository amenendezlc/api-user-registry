package com.javatest.userregistry.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneDto {
	private Long id;
	private Long number;
	private int cityCode;
	private int countryCode;
}
