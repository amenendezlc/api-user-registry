package com.javatest.userregistry.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    @NotBlank(message = "El nombre no debe estar vacio")
    private String name;
    @Pattern(regexp=".+@.+\\.[a-z]+",
            message = "El email debe debe tener el formato correcto")
    private String email;
    @Pattern(regexp="^(?=((?:.*[A-Z]){1,1}))(?=.*[a-z])(?=(?:.*?\\d){2,2})[a-zA-Z0-9]{4,}$",
            message = "La clave debe tener el formato correcto")
    private String password;
    private List<PhoneDto> phones;
    private Date created;
    private Date modified;
    private Date lastLogin;
    private String token;
    private boolean active;
}
