package com.javatest.userregistry.exceptions.handlers;

import com.javatest.userregistry.dto.MessageResponse;
import com.javatest.userregistry.exceptions.UserAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UserAlreadyExistsException.class})
    protected ResponseEntity<Object> handleUserAlreadyExists(
            UserAlreadyExistsException ex) {
        log.info(ex.getMessage());
        MessageResponse body = new MessageResponse("El correo ya registrado");
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        List<MessageResponse> body = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> {
                    log.info("Validacion del objeto \"{}\" -> mensaje validacion: \"{}\" -> campo \"{}\", valor rechazado: {}",
                            x.getObjectName(), x.getDefaultMessage(), x.getField(), x.getRejectedValue());
                    return new MessageResponse(x.getDefaultMessage());
                })
                .collect(Collectors.toList());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleInternalServerError(
            Exception ex) {
        String exceptionMessage = ex.getMessage() == null ? "Ocurrio un error inesperado"
                : ex.getMessage();
        log.error(exceptionMessage, ex);
        MessageResponse body = new MessageResponse("Ocurrio un error en el sistema al realizar la operacion");
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
