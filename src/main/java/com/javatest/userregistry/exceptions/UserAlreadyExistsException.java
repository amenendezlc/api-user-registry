package com.javatest.userregistry.exceptions;

public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException(String fieldName, String fieldValue) {
        super(String.format("Usuario se encuentra registrado. Campo: \"%s\", Valor: %s",
                fieldName, fieldValue));
    }
}
