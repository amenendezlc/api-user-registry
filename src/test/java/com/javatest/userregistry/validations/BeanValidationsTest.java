package com.javatest.userregistry.validations;

import com.javatest.userregistry.dto.UserDto;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class BeanValidationsTest {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeAll
    static void setUp() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    void shouldHaveNoValidationViolations() {
        UserDto user = new UserDto();
        user.setName("Andres");
        user.setEmail("andres@corp.com");
        user.setPassword("Aoij22");

        Set<ConstraintViolation<UserDto>> violations
                = validator.validate(user);

        assertTrue(violations.isEmpty());
    }

    @Test
    void shouldDetectInvalidEmail() {
        UserDto user = new UserDto();
        user.setName("Andres");
        user.setEmail("andrescorp.com");
        user.setPassword("Aoij22");

        Set<ConstraintViolation<UserDto>> violations
                = validator.validate(user);

        ConstraintViolation<UserDto> violation
                = violations.iterator().next();

        assertAll(
                () -> assertEquals(violations.size(), 1),
                () -> assertEquals("El email debe debe tener el formato correcto", violation.getMessage()),
                () -> assertEquals("email", violation.getPropertyPath().toString()),
                () -> assertEquals("andrescorp.com", violation.getInvalidValue())
        );
    }

    @Test
    void shouldDetectInvalidPassword() {
        UserDto user = new UserDto();
        user.setName("Andres");
        user.setEmail("andres@corp.com");
        user.setPassword("oij22");

        Set<ConstraintViolation<UserDto>> violations
                = validator.validate(user);

        ConstraintViolation<UserDto> violation
                = violations.iterator().next();

        assertAll(
                () -> assertEquals(violations.size(), 1),
                () -> assertEquals("La clave debe tener el formato correcto", violation.getMessage()),
                () -> assertEquals("password", violation.getPropertyPath().toString()),
                () -> assertEquals("oij22", violation.getInvalidValue())
        );
    }

    @Test
    void shouldHDetectInvalidName() {
        UserDto user = new UserDto();
        user.setName(null);
        user.setEmail("andres@corp.com");
        user.setPassword("Aoij22");

        Set<ConstraintViolation<UserDto>> violations
                = validator.validate(user);

        ConstraintViolation<UserDto> violation
                = violations.iterator().next();

        assertAll(
                () -> assertEquals(violations.size(), 1),
                () -> assertEquals("El nombre no debe estar vacio", violation.getMessage()),
                () -> assertEquals("name", violation.getPropertyPath().toString()),
                () -> assertNull(violation.getInvalidValue())
        );
    }

    @Test
    void shouldDetectInvalidUserDtoObject() {
        UserDto user = new UserDto();
        user.setName(null);
        user.setEmail("andrescorp.com");
        user.setPassword("oij22");

        Set<ConstraintViolation<UserDto>> violations
                = validator.validate(user);

        assertEquals(3, violations.size());
    }

    @AfterAll
    public static void close() {
        validatorFactory.close();
    }
}
