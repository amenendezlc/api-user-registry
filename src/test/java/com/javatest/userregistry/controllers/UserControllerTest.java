package com.javatest.userregistry.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javatest.userregistry.dto.UserDto;
import com.javatest.userregistry.models.UserEntity;
import com.javatest.userregistry.services.UserServiceImpl;
import com.javatest.userregistry.services.mappings.MapperService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	UserServiceImpl userService;

	@MockBean
	MapperService<UserDto, UserEntity> userMappingService;

	@Test
	void whenValidInput_thenReturnCreated() throws Exception {
		UserDto userDto = new UserDto();
		userDto.setName("Alonzo Marti");
		userDto.setEmail("alonzo@gmail.com");
		userDto.setPassword("Pass22");

		mockMvc.perform(post("/user")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(userDto)))
				.andExpect(status().isCreated());
	}

	@Test
	void whenValidInput_thenReturnBadRequest() throws Exception {
		UserDto userDto = new UserDto();
		userDto.setName(null);
		userDto.setEmail("alonzo@gmail.com");
		userDto.setPassword("Pass22");

		mockMvc.perform(post("/user")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(userDto)))
				.andExpect(status().isBadRequest());
	}

	@Test
	void whenValidInput_thenCorrectUserDtoArgument() throws Exception {
		UserDto userDto = new UserDto();
		userDto.setName("Alonzo Marti");
		userDto.setEmail("alonzo@gmail.com");
		userDto.setPassword("Pass22");

		mockMvc.perform(post("/user")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(userDto)))
				.andExpect(status().isCreated());

		ArgumentCaptor<UserDto> userDtoCaptor = ArgumentCaptor.forClass(UserDto.class);
		verify(userMappingService, times(1)).convertToEntity(userDtoCaptor.capture());

		assertEquals("Alonzo Marti", userDtoCaptor.getValue().getName());
		assertEquals("alonzo@gmail.com", userDtoCaptor.getValue().getEmail());
	}

	@Test
	void whenValidInput_thenReturnsUserResource() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setName("Alonzo Marti");
        userDto.setEmail("alonzo@gmail.com");
        userDto.setPassword("Pass22");

		UserEntity convertedUserEntity = new UserEntity();
        convertedUserEntity.setName("Alonzo Marti");
        convertedUserEntity.setEmail("alonzo@gmail.com");
        convertedUserEntity.setPassword("Pass22");

        UserEntity userEntityOut = new UserEntity();
        userEntityOut.setId(3L);
        userEntityOut.setName("Alonzo Marti");
        userEntityOut.setEmail("alonzo@gmail.com");
        userEntityOut.setPassword("Pass22");

        UserDto userDtoOut = new UserDto();
        userDtoOut.setId(3L);
        userDtoOut.setName("Alonzo Marti");
        userDtoOut.setEmail("alonzo@gmail.com");
        userDtoOut.setPassword("Pass22");

        when(userMappingService.convertToEntity(any(UserDto.class))).thenReturn(convertedUserEntity);
		when(userService.registerUser(any(UserEntity.class))).thenReturn(userEntityOut);
        when(userMappingService.convertToDto(any(UserEntity.class))).thenReturn(userDtoOut);

		MvcResult mvcResult = mockMvc.perform(post("/user")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(userDto)))
				.andExpect(status().isCreated())
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		UserDto userDtoResponse = objectMapper.readValue(actualResponseBody,UserDto.class);

		assertAll(
		        () -> assertEquals(3L,userDtoResponse.getId()),
                () -> assertEquals(userDto.getName(), userDtoResponse.getName())
        );
	}


}