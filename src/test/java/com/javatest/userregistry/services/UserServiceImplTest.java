package com.javatest.userregistry.services;

import com.javatest.userregistry.exceptions.UserAlreadyExistsException;
import com.javatest.userregistry.models.PhoneEntity;
import com.javatest.userregistry.models.UserEntity;
import com.javatest.userregistry.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtService jwtService;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    private UserEntity userEntityInput;

    @BeforeEach
    void setup() {
        // Input UserService Test
        List<PhoneEntity> phonesInput = new ArrayList<>();
        phonesInput.add(new PhoneEntity(null, 9879872L, 1, 2));
        phonesInput.add(new PhoneEntity(null, 2877882L, 2, 2));
        userEntityInput = new UserEntity(null, "Alonzo Gomez", "alonzo@unit1.com",
                "Pass22", phonesInput, null, null, null, null, false);

        // Output UserService Test
        List<PhoneEntity> phoneEntityOutput = new ArrayList<>();
        phoneEntityOutput.add(new PhoneEntity(3L, 9879872L, 1, 2));
        phoneEntityOutput.add(new PhoneEntity(4L, 2877882L, 2, 2));
        UserEntity userEntityOutput = new UserEntity(2L, "Alonzo Gomez", "alonzo@unit1.com",
                "Pass22", phoneEntityOutput, new Date(), new Date(), new Date(), "ANY_TOKEN", true);

        // When JWT generator is called
        lenient().when(jwtService.generateToken(anyString(),any(Date.class))).thenReturn("ANY_TOKEN");

        // When UserRepository is called
        lenient().when(userRepository.save(any(UserEntity.class))).thenReturn(userEntityOutput);
    }

    @Test
    void when_saveValidUser() {
        UserEntity assertedUserEntity = userServiceImpl.registerUser(userEntityInput);
        assertAll(
                () -> assertEquals(2L, assertedUserEntity.getId()),
                () -> assertEquals("ANY_TOKEN", assertedUserEntity.getToken()),
                () -> assertNotNull(assertedUserEntity.getCreated()),
                () -> assertNotNull(assertedUserEntity.getModified()),
                () -> assertNotNull(assertedUserEntity.getLastLogin()),
                () -> assertNotNull(assertedUserEntity.getPhones()),
                () -> assertEquals(2, assertedUserEntity.getPhones().size()),
                () -> assertEquals(3L , assertedUserEntity.getPhones().get(0).getId()),
                () -> assertEquals(4L , assertedUserEntity.getPhones().get(1).getId()));
    }

    @Test
    void when_saveExistingUser() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(new UserEntity()));
        UserAlreadyExistsException ex = assertThrows(UserAlreadyExistsException.class,
                () -> userServiceImpl.registerUser(userEntityInput));
        assertTrue(ex.getMessage().contains("Usuario se encuentra registrado"));
    }
}