package com.javatest.userregistry.services;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class JwtServiceImplTest {

    private static String validToken;
    private static Date validDate;
    private static String validEmail;
    private static JwtServiceImpl jwtServiceImpl;

    @BeforeAll
    static void setup() {
        String secret = "5163B7AF994CD340D273A4247575880D6ACAB8339F638BB16674BE80156A79A9";
        jwtServiceImpl = new JwtServiceImpl(secret);
        validEmail = "registeradmin1@gmail.com";
        validDate = new Date();
        validToken = jwtServiceImpl.generateToken(validEmail,validDate);
    }

    @Test
    void given_validEmail_when_generateToken() {
        assertAll(
                () -> assertNotNull(validToken),
                () -> assertEquals(validToken, jwtServiceImpl.generateToken(validEmail,validDate)));
    }

    @Test
    void given_otherEmail_when_generateDiffTokens() {
        String otherEmail = "test@anotheremail.com";
        assertAll(
                () -> assertNotNull(jwtServiceImpl.generateToken(otherEmail,validDate)),
                () -> assertNotEquals(validToken, jwtServiceImpl.generateToken(otherEmail,validDate))
        );
    }
}