# Simple User Registry

This project scopes a single REST API endpoint for user registration with a JWT Token.

- [Component Diagram](user-registration-component-diagram.jpg)
- [Sequence Diagram](user-registration-sequence-diagram.jpg)

## Prerequisites

You need Java 8 installed

## Usage

- If you want to run the unit test suite (and integration test with MockMvc):

```
gradle clean test
```

1. To run the project, got to the project root directory and execute the following in terminal:

```
gradle bootRun
```

2. To register a user, execute:

```
curl --request POST \
  --url http://localhost:8080/user \
  --header 'content-type: application/json' \
  --data '{
	"name" : "Juan Rodriguez" ,
	"email" : "juan@rodriguez.org" ,
	"password" : "Paxds22" ,
	"phones" : [
	{
	"number" : "1234567" ,
	"citycode" : "1" ,
	"contrycode" : "57"
	}
	]
}'
```

- *Note:* There is a deafult register of a user in [data.sql](src/main/resources/data.sql), for testing purposes.

```
INSERT INTO USER (NAME,EMAIL,PASSWORD,ACTIVE) VALUES('Richard Tapias','registeradmin@gmail.com','Padmin21',TRUE);
```

## Deliverable required aspects

- Use of H2 Database
- Build and dependency management with Gradle
- Hibernate / JPA Persistence
- SpringBoot 2.2.4
- Published on BitBucket
- UML Component and Sequence diagrams
- This MD file with instructions
- Unit testing with JUnit 5, Mockito and MockMvc
- JWT Token

## References

- JWT - [Verified java library](https://github.com/jwtk/jjwt)
